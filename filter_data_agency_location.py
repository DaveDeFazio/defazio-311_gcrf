#https://www.incomebyzipcode.com/newyork/11368
#https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/7ahn-ypff

"""
How I am filtering:

1. Remove rows that dont have valid open/close dates
2. Only include calls from a top 8 agency
3. The borogh must be specified
4. Change outliers to the median (an oulier is +-(8*median), or < 0)
5. Each file produced is for a specific agency and borough combo, denoted in the title of the file
The files produced has the median response times in hours, corresponding to the day

"""

import pandas as pd
import matplotlib.pyplot as plt
import csv
import sys
from datetime import datetime
import statistics as stat

#return a generator, so we dont have to load the whole file in memory
def read_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        for row in reader:
            yield row

#2012 and 2016 are leap years
def getDayNumber(year, month, day):
	year = year-2010 #2010 is the first year we have data for
	days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31]
	day_num = (year*365)
	if((year > 2) or ((year == 2) and (month > 2))): #2012 leap year
		day_num += 1
	if((year > 6) or ((year == 6) and (month > 2))): #2016 leap year
		day_num += 1
	for i in list(range(0,month-1)):
		day_num += days_in_months[i]
	day_num += day
	return(day_num)

def subtractDates(start,end):
	format = '%m/%d/%Y %I:%M:%S %p'
	start_date = datetime.strptime(start, format)
	end_date = datetime.strptime(end, format)
	difference = (end_date-start_date).total_seconds()
	return int(difference)

#366 days in 2012 and 2016, 365 days in 2010, 2011, 2013, 2014, 2015, and 2017. Also using 58 days of 2018
top_agencies = ['DOHMH','NYPD','DSNY','DEP','DPR','DOB','DOT','HPD']
boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

total_num_days = (366*2)+(365*6)+58

#initialize dictionary of dictionaries to store all time series
all_time_series_response = {borough+'_'+agency:{x:[] for x in range(0,total_num_days)} 
	for borough in boroughs for agency in top_agencies}
all_time_series_demand = {borough+'_'+agency:{x:0 for x in range(0,total_num_days)} 
	for borough in boroughs for agency in top_agencies}

print("Finding Response Times...")

#store each calls response time and demand in appropriate dictionary
file_generator = read_csv('311_data.csv')

for line in file_generator:
	if(line[1] != 'Created Date'):
		created_date = line[1]
		closed_date = line[2]
		agency = line[3]
		borough = line[24]
		if(closed_date != '' and agency in top_agencies
			and borough != 'Unspecified'): #ignore rows that have no closed date, or not a top 8 agency
			response_time = int(subtractDates(created_date,closed_date)/3600) #in hours
			year = int(created_date.split()[0].split('/')[2])
			month = int(created_date.split()[0].split('/')[0])
			day = int(created_date.split()[0].split('/')[1])
			inner_key = getDayNumber(year,month,day)-1 #day index of call
			outer_key = borough+'_'+agency #location and agency of call
			all_time_series_response[outer_key][inner_key].append(response_time)
			all_time_series_demand[outer_key][inner_key] += 1

#now replace each response time list with its median value
for borough in boroughs:
	for agency in top_agencies:
		median = 0
		for i in range(0,total_num_days):
			outer_key = borough+'_'+agency
			inner_key = i
			if(len(all_time_series_response[outer_key][inner_key]) > 0):
				median = stat.median(all_time_series_response[outer_key][inner_key])
			all_time_series_response[outer_key][inner_key] = []
			all_time_series_response[outer_key][inner_key].append(median)

time = [x for x in range(1,1+total_num_days)]

#write to csv
path = '/home/mllab/Desktop/defazio-311_gcrf/dataByAgencyLocation/' #write data here
fh = open("location_agency_stats.txt","w") #write statistics here
for outer_key in all_time_series_response.keys():
	response_times = []
	demands = []

	#inner key must be in order
	for inner_key in all_time_series_response[outer_key].keys():
		response_times.append(all_time_series_response[outer_key][inner_key][0])
		demands.append(all_time_series_demand[outer_key][inner_key])

	#change outliers in response and demand to the median
	median_response = stat.median(response_times)
	median_demand = stat.median(demands)
	num_response_outliers = 0
	num_demand_outliers = 0
	for k in range(0,len(response_times)):
		if((response_times[k] > 8*median_response) or (response_times[k] < 0)):
			response_times[k] = median_response
			num_response_outliers += 1
		if((demands[k] > 8*median_demand) or (demands[k] < 0)):
			demands[k] = median_demand
			num_demand_outliers += 1


	fh.write(outer_key+'\n')
	fh.write("Response outliers: "+str(num_response_outliers)+'\n')
	fh.write("Demand outliers: "+str(num_demand_outliers)+'\n')
	fh.write("Number of total calls: "+str(sum(demands))+'\n\n')

	"""plt.plot(time, response_times)
	plt.plot(time, demands)
	plt.xlabel('Day')
	plt.ylabel('Median Response Time (blue) and Demands (orange)')
	plt.title('311 Median Response Times and Demands for 2010 - 2018')
	plt.show()"""
	data = pd.DataFrame({'Response_Time':response_times, 'Demand':demands})
	print("writing csv...")
	data.to_csv(path+outer_key + '.csv')

fh.close()


#list of lists of dictionaries
"""list_of_response_dicts = [ [] for x in range(0,total_num_locations)]
list_of_demand_dicts = [[] for x in range(0,total_num_locations)]

for i in range(0,len(list_of_response_dicts)):
	for j in top_agencies:
		list_of_response_dicts[i].append({j+str(x):[] for x in range(1,1+total_num_days)})
		list_of_demand_dicts[i].append({j+str(x):0 for x in range(1,1+total_num_days)})

file_generator = read_csv('311_data.csv')
csv_length = 0
major_agency_calls = 0

#index 1 is created date
#index 2 is closed date
#index x is borough
#FILTER BY BOROUGH COLUMN
for line in file_generator: #index 1 is created date
	if(line[1] != 'Created Date'):
		csv_length += 1
		created_date = line[1]
		closed_date = line[2]
		agency = line[3]
		borough = line[24]
		if(agency in top_agencies):
			major_agency_calls += 1
		if(closed_date != '' and agency in top_agencies): #ignore rows that have no closed date, or not a top 8 agency
			response_time = int(subtractDates(created_date,closed_date)/3600) #in hours
			year = int(created_date.split()[0].split('/')[2])
			month = int(created_date.split()[0].split('/')[0])
			day = int(created_date.split()[0].split('/')[1])
			day_num = getDayNumber(year,month,day)
			for index_borough, item_borough in enumerate(boroughs):
				if(borough == item_borough):
					for index_agency,item_agency in enumerate(top_agencies):
						if(agency == item_agency):
							list_of_response_dicts[index_borough][index_agency][agency+str(day_num)].append(response_time)
							list_of_demand_dicts[index_borough][index_agency][agency+str(day_num)] += 1

print("Total number of rows read:",csv_length)
print("Major agency calls:",major_agency_calls)
print(major_agency_calls/csv_length)


#now replace each response time list with its median value
for i,lst in enumerate(list_of_response_dicts):
	for d in lst:
		num_days_no_data = 0
		for key,value in d.items():
			median = 0
			if(len(value) > 0):
				median = stat.median(value)
			else:
				num_days_no_data += 1
			d[key] = []
			d[key].append(median)
		print("No data for",num_days_no_data,"total days in",boroughs[i])

#extract a list of ordered response times from each dictionary, and write to csv
time = [x for x in range(1,1+total_num_days)]

for i in range(0, len(boroughs)):
	for j in range(0,len(top_agencies)):
		response_times = []
		demands = []
		index = 0
		agency = ''
		for key in sorted(list_of_response_dicts[j]):
			if(index == 0):
				agency = key[:-1]
			response_times.append(list_of_response_dicts[i][j][key][0])
			demands.append(list_of_demand_dicts[i][j][key])
			index += 1

		#change outliers in response and demand to the median
		median_response = stat.median(response_times)
		median_demand = stat.median(demands)
		num_response_outliers = 0
		num_demand_outliers = 0
		for k in range(0,len(response_times)):
			if((response_times[k] > 8*median_response) or (response_times[k] < 0)):
				response_times[k] = median_response
				num_response_outliers += 1
			if((demands[k] > 8*median_demand) or (demands[k] < 0)):
				demands[k] = median_demand
				num_demand_outliers += 1

		print("Response outliers:",num_response_outliers)
		print("Demand outliers:",num_demand_outliers)

		print("Agency: "+agency)
		print("Borough: "+boroughs[i])
		plt.plot(time, response_times)
		plt.plot(time, demands)
		plt.xlabel('Day')
		plt.ylabel('Median Response Time (blue) and Demands (orange)')
		plt.title('311 Median Response Times and Demands for 2010 - 2018')
		plt.show()
		data = pd.DataFrame({'Response_Time':response_times, 'Demand':demands, 'Day':time})
		print("writing csv...")
		data.to_csv(boroughs[i] + '_' + agency + '_Response_Demand.csv')
"""