import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs

def multiplyEveryOther(lst):
	newLst = []
	for i in range(0,len(lst),2):
		if(i+1 < len(lst)):
			newLst.append(lst[i]*lst[i+1])
	return newLst

x_length = 50
y_length = 7
iterations = 10
agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']

for chosen_agency in agencies:
	print("Running for:",chosen_agency)
	response_times, X_train, Y_train, X_test, Y_test = gs.getSamplesWithDemand(
		chosen_agency, x_length, y_length)

	avg_response_demand = np.mean(Y_train, axis=0) #take mean response and demand at each time step
	avg_response = []
	avg_demand = []
	for index,item in enumerate(avg_response_demand):
		if(index%2 == 0):
			avg_response.append(item)
		else:
			avg_demand.append(item)

	#multiply every other element together
	X_train_aggregate = np.zeros([X_train.shape[0],x_length])
	for index,row in enumerate(X_train):
		X_train_aggregate[index,:] = multiplyEveryOther(row.tolist())

	Y_train_aggregate = np.zeros([Y_train.shape[0],y_length])
	for index,row in enumerate(Y_train):
		Y_train_aggregate[index,:] = multiplyEveryOther(row.tolist())

	X_test_aggregate = np.zeros([X_test.shape[0],x_length])
	for index,row in enumerate(X_test):
		X_test_aggregate[index,:] = multiplyEveryOther(row.tolist())

	model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000) #lamL and lamT are regularization parameters

	mses = []
	maes = []
	for i in range(0,iterations):
		model.fit(X_train_aggregate, Y_train_aggregate)
		aggregate_predictions = model.predict(X_test_aggregate)

		#decompose predictions back into response and demand
		decomposed_responses = np.divide(aggregate_predictions, avg_demand)
		decomposed_demands = np.divide(aggregate_predictions, avg_response)

		Y_test_response = Y_test[:,::2] #all odd columns
		Y_test_demand = Y_test[:,1::2] #all even columns

		mse = mean_squared_error(decomposed_responses, Y_test_response)
		mae = mean_absolute_error(decomposed_responses, Y_test_response)
		mses.append(mse)
		maes.append(mae)

	avg_mse = sum(mses)/len(mses)
	avg_mae = sum(maes)/len(maes)

	print("Average MSE across", iterations, "iterations:",avg_mse)
	print("Average MAE across", iterations, "iterations:",avg_mae)

"""days = [x for x in range(0,len(response_times))]

plt.plot(days, aggregate)
plt.xlabel('Day')
plt.ylabel('Response times')
plt.title('median response time per day for particular agency')
plt.show()"""