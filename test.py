import numpy as np

theta = np.ones((50, 7))
lam = np.ones((7,7))

lam = np.triu(lam,-1)
lam = np.tril(lam,1)

keep_index = 6
for i in range(0,theta.shape[0]-1):
	theta[i, :keep_index] = 0
	theta[i, keep_index+1:] = 0
	if(keep_index == 6):
		keep_index = 0
	else:
		keep_index += 1

theta[-1,1:6] = 0

print(theta)