import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs


x_length = 50
y_length = 7
iterations = 3

agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']
boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByAgencyLocation/'

#run for each borough seperately
for chosen_borough in boroughs:

	print("Borough:",chosen_borough)

	filename = path + chosen_borough + '_DEP' + '.csv'
	_, dep_x_train, dep_y_train, dep_x_test, dep_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_DOB' + '.csv'
	_, dob_x_train, dob_y_train, dob_x_test, dob_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_DOHMH' + '.csv'
	_, dohmh_x_train, dohmh_y_train, dohmh_x_test, dohmh_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_DOT' + '.csv'
	_, dot_x_train, dot_y_train, dot_x_test, dot_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_DPR' + '.csv'
	_, dpr_x_train, dpr_y_train, dpr_x_test, dpr_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_DSNY' + '.csv'
	_, dsny_x_train, dsny_y_train, dsny_x_test, dsny_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_HPD' + '.csv'
	_, hpd_x_train, hpd_y_train, hpd_x_test, hpd_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + chosen_borough + '_NYPD' + '.csv'
	_, nypd_x_train, nypd_y_train, nypd_x_test, nypd_y_test = gs.getSamples(
			filename, x_length, y_length)

	

	X_train = np.empty([dep_x_train.shape[0], 
		dep_x_train.shape[1] + dob_x_train.shape[1] + dohmh_x_train.shape[1]
		+ dot_x_train.shape[1] + dpr_x_train.shape[1] + dsny_x_train.shape[1]
		+ hpd_x_train.shape[1] + nypd_x_train.shape[1]])

	Y_train = np.empty([dep_y_train.shape[0], 
		dep_y_train.shape[1] + dob_y_train.shape[1] + dohmh_y_train.shape[1]
		+ dot_y_train.shape[1] + dpr_y_train.shape[1] + dsny_y_train.shape[1]
		+ hpd_y_train.shape[1] + nypd_y_train.shape[1]])

	X_test = np.empty([dep_x_test.shape[0], 
		dep_x_test.shape[1] + dob_x_test.shape[1] + dohmh_x_test.shape[1]
		+ dot_x_test.shape[1] + dpr_x_test.shape[1] + dsny_x_test.shape[1]
		+ hpd_x_test.shape[1] + nypd_x_test.shape[1]])


	X_train[:,0::8] = dep_x_train
	X_train[:,1::8] = dob_x_train
	X_train[:,2::8] = dohmh_x_train
	X_train[:,3::8] = dot_x_train
	X_train[:,4::8] = dpr_x_train
	X_train[:,5::8] = dsny_x_train
	X_train[:,6::8] = hpd_x_train
	X_train[:,7::8] = nypd_x_train

	Y_train[:,0::8] = dep_y_train
	Y_train[:,1::8] = dob_y_train
	Y_train[:,2::8] = dohmh_y_train
	Y_train[:,3::8] = dot_y_train
	Y_train[:,4::8] = dpr_y_train
	Y_train[:,5::8] = dsny_y_train
	Y_train[:,6::8] = hpd_y_train
	Y_train[:,7::8] = nypd_y_train

	X_test[:,0::8] = dep_x_test
	X_test[:,1::8] = dob_x_test
	X_test[:,2::8] = dohmh_x_test
	X_test[:,3::8] = dot_x_test
	X_test[:,4::8] = dpr_x_test
	X_test[:,5::8] = dsny_x_test
	X_test[:,6::8] = hpd_x_test
	X_test[:,7::8] = nypd_x_test


	model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000)

	dep_mses = []
	dob_mses = []
	dohmh_mses = []
	dot_mses = []
	dpr_mses = []
	dsny_mses = []
	hpd_mses = []
	nypd_mses = []

	for i in range(0,iterations):

		model.fit(X_train, Y_train)
		predictions = model.predict(X_test)

		#every 8th y belongs to the same agency
		dep_prediction = predictions[:,0::8]
		dob_prediction = predictions[:,1::8]
		dohmh_prediction = predictions[:,2::8]
		dot_prediction = predictions[:,3::8]
		dpr_prediction = predictions[:,4::8]
		dsny_prediction = predictions[:,5::8]
		hpd_prediction = predictions[:,6::8]
		nypd_prediction = predictions[:,7::8]


		dep_mses.append(mean_squared_error(dep_prediction, dep_y_test))
		dob_mses.append(mean_squared_error(dob_prediction, dob_y_test))
		dohmh_mses.append(mean_squared_error(dohmh_prediction, dohmh_y_test))
		dot_mses.append(mean_squared_error(dot_prediction, dot_y_test))
		dpr_mses.append(mean_squared_error(dpr_prediction, dpr_y_test))
		dsny_mses.append(mean_squared_error(dsny_prediction, dsny_y_test))
		hpd_mses.append(mean_squared_error(hpd_prediction, hpd_y_test))
		nypd_mses.append(mean_squared_error(nypd_prediction, nypd_y_test))

	print("DEP:",sum(dep_mses)/iterations)
	print("DOB:",sum(dob_mses)/iterations)
	print("DOHMH:",sum(dohmh_mses)/iterations)
	print("DOT:",sum(dot_mses)/iterations)
	print("DPR:",sum(dpr_mses)/iterations)
	print("DSNY:",sum(dsny_mses)/iterations)
	print("HPD:",sum(hpd_mses)/iterations)
	print("NYPD:",sum(nypd_mses)/iterations)


"""
>>> import numpy as np
>>> a = np.array([[1,4,7],[1,4,7]])
>>> b = np.array([[2,5,8],[2,5,8]])
>>> c = np.array([[3,6,9],[3,6,9]])
>>> d = np.empty([a.shape[0],a.shape[1]+b.shape[1]+c.shape[1]])
>>> d[:,0::3] = a
>>> d[:,1::3] = b
>>> d[:,2::3] = c
>>> d
array([[1., 2., 3., 4., 5., 6., 7., 8., 9.],
       [1., 2., 3., 4., 5., 6., 7., 8., 9.]])

"""

























#Model all agencies in  the same graph
"""import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error

print("Reading data...")

#take response data from all agencies
response_times = np.genfromtxt('All_Agencies_Response.csv', delimiter=',')
days = response_times[1:,0]
agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']
response_times = response_times[1:,1:] #ignore header and id column

y_length = 7 #predict the final y_length number of time indicies (per agency)
x_length = 50 #use x_length number of historical time indicies (per agency)

train_percent = 0.75

num_train_rows = int(train_percent*len(days))
num_test_rows = len(days) - num_train_rows

num_train_samples = num_train_rows-(x_length+y_length)+1
num_test_samples = num_test_rows-(x_length+y_length)+1
print("Number of training samples:",num_train_samples)
print("Number of testing samples:",num_test_samples)

train_data = response_times[:num_train_rows,:] #dont look at last y_length data points for training
test_data = response_times[-(num_test_rows):,:]

#construct train samples
X_train = []
Y_train = []
for i in range(0, num_train_samples):
	x = train_data[i:i+x_length,:] #get all data for a particular train sequence
	y = train_data[i+x_length:i+x_length+y_length,:] #get all labels for associated train sequence
	x_seq = x.flatten().tolist() #stack columns to 1d array
	y_seq = y.flatten().tolist() #stack columns to 1d array
	X_train.append(x_seq)
	Y_train.append(y_seq)

#construct test sample
X_test = []
Y_test = []	
for i in range(0, num_test_samples):
	x = test_data[i:i+x_length,:] #get all data for a particular train sequence
	y = test_data[i+x_length:i+x_length+y_length,:] #get all labels for associated train sequence
	x_seq = x.flatten().tolist() #stack columns to 1d array
	y_seq = y.flatten().tolist() #stack columns to 1d array
	X_test.append(x_seq)
	Y_test.append(y_seq)

X_train = np.array(X_train)
Y_train = np.array(Y_train)
X_test = np.array(X_test)
Y_test = np.array(Y_test)

model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter = 10000)
print("Fitting GCRF...")
model.fit(X_train, Y_train)
prediction = model.predict(X_test)

#find MSE for each agency
index = 0
for item in agencies:
	#there should be len(agencies)*y_length total predictions
	agency_predictions = prediction[index: index+y_length]
	agency_truth = Y_test[index: index+y_length]
	mse = mean_squared_error(agency_predictions, agency_truth)
	print("MSE for",item, ":", mse)
	index += y_length"""

"""forecast = train_data.tolist()
for i in prediction:
	forecast.append(i)"""


"""plt.plot(days, response_times)
plt.plot(days, forecast)
plt.xlabel('Day')
plt.ylabel('Response times')
plt.title('median response time per day for particular agency')
plt.show()"""