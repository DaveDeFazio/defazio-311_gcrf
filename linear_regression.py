import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs
from sklearn import linear_model
import sys

x_length = 50
y_length = 7

#make a best fit line on each test sample (dont even use training data)
agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']
boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByAgencyLocation/'


#run for each agency/borough seperately
for chosen_borough in boroughs:
	for chosen_agency in agencies:
		filename = path+chosen_borough+'_'+chosen_agency+'.csv'
		print("Running for:",chosen_borough,'|',chosen_agency)
		_, __, ___, X_test, Y_test = gs.getSamples(filename, x_length, y_length)


#days = [i for i in range(0,x_length+y_length)]

		model = linear_model.LinearRegression()
		x = [i for i in range(0,x_length)]
		y = [i for i in range(x_length,x_length+y_length)]
		x_days = np.array(x)
		y_days = np.array(y)
		x_days = x_days.reshape((-1,1))
		y_days = y_days.reshape((-1,1))
		all_mse = []
		all_mae = []

		#for each test sample, fit a straight line and calculate the MSE
		for i in range(0, X_test.shape[0]):
			model.fit(x_days,X_test[i,:].reshape((-1,1)))
			prediction = model.predict(y_days)
			mse = mean_squared_error(prediction, Y_test[i,:].reshape((-1,1)))
			mae = mean_absolute_error(prediction, Y_test[i,:].reshape((-1,1)))
			all_mse.append(mse)
			all_mae.append(mae)


		avg_mse = sum(all_mse)/len(all_mse)
		avg_mae = sum(all_mae)/len(all_mae)
		print("Average Response MSE:",avg_mse)
		print("Average Response MAE:",avg_mae)

#plot the last sample
"""truth = np.concatenate((X_test[-1,:],Y_test[-1,:]))
prediction = np.array(prediction).reshape((1,-1))[0,:]
print(prediction)
forecast = np.concatenate((X_test[-1,:],prediction))
plt.plot(days, truth)
plt.plot(days, forecast)
plt.xlabel('Day')
plt.ylabel('Response Times (in hours)')
plt.title('Median Response Time per Day for DOT')
plt.show()
sys.exit()"""

"""
Average MSE for DOT : 532.4179005600547
Average MAE for DOT : 17.60263094513046
"""