import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs

#subtract by mean and divide by std (mean and std of all response times)
def normalize(data):
	std = np.std(response_times)
	mean = np.mean(response_times)
	for i in range(0,data.shape[0]):
		for j in range(0,data.shape[1]):
			data[i,j] = (data[i,j]-mean)/std
	return data

#multiply by std and add the mean (mean and std of all response times)
def denormalize(data):
	std = np.std(response_times)
	mean = np.mean(response_times)
	for i in range(0,data.shape[0]):
			for j in range(0,data.shape[1]):
				data[i,j] = ((std*data[i,j])+mean)
	return data


x_length = 50
y_length = 7
iterations = 2

complaints = ['APPLIANCE','Air Quality','Animal Abuse', 'Animal in a Park',
			'Asbestos', 'Blocked Driveway', 'Broken Muni Meter', 'Building_Use', 
			'Consumer Complaint', 'Curb Condition', 
			'DCA _ DOH New License Application Request','DOF Literature Request', 
			'DOF Parking - Payment Issue', 'DOF Property - Owner Issue', 
			'DOF Property - Payment Issue', 'DOF Property - Reduction Issue', 
			'DOF Property - Request Copy', 'DOF Property - Update Account', 
			'DOOR_WINDOW', 'Damaged Tree', 'Dead_Dying Tree', 'Derelict Vehicle',
			'Dirty Conditions', 'ELECTRIC', 'Electrical', 'Elevator', 'FLOORING_STAIRS', 
			'Food Establishment', 'Food Poisoning', 'For Hire Vehicle Complaint', 
			'GENERAL', 'General Construction_Plumbing', 'Graffiti', 'HEAT_HOT WATER', 
			'HPD Literature Request', 'Hazardous Materials', 'Highway Condition', 
			'Homeless Encampment', 'Housing - Low Income Senior', 'Illegal Parking', 
			'Indoor Air Quality', 'Lead', 'Litter Basket _ Request',
			'Maintenance or Facility', 'Missed Collection (All Materials)',
			'New Tree Request', 'Noise - Commercial', 'Noise - Park', 
			'Noise - Residential', 'Noise - Street_Sidewalk', 'Noise - Vehicle',
			'Noise', 'Non-Emergency Police Matter', 'Other Enforcement',
			'Overgrown Tree_Branches', 'PAINT_PLASTER', 'PLUMBING', 'Plumbing', 'Rodent', 
			'Root_Sewer_Sidewalk Condition', 'SAFETY', 'SCRIE', 'Sanitation Condition',
			'Sewer', 'Sidewalk Condition', 'Special Projects Inspection Team (SPIT)',
			'Standing Water', 'Street Condition', 'Street Sign - Damaged', 
			'Street Sign - Dangling', 'Street Sign - Missing', 'Taxi Complaint', 
			'Traffic Signal Condition', 'Traffic', 'UNSANITARY CONDITION', 
			'Unsanitary Animal Pvt Property', 'Vacant Lot', 'Vending', 
			'WATER LEAK', 'Water Conservation', 'Water System']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaint/'

#run for each agency/borough seperately
for chosen_complaint in complaints:

	filename = path + chosen_complaint +'.csv'
	print("Running for:",chosen_complaint)

	response_times, X_train, Y_train, X_test, Y_test = gs.getSamples(
		filename, x_length, y_length)

	#days = [x for x in range(0,len(response_times))]


	#X_train = normalize(X_train)
	#Y_train = normalize(Y_train)
	#X_test = normalize(X_test)

	model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000) #lamL and lamT are regularization parameters

	mses = []
	maes = []

	#run the model a few times and average the MSE
	for i in range(0,iterations):

		model.fit(X_train, Y_train)
		prediction = model.predict(X_test)
		#prediction = denormalize(prediction)

		#matrix MSE
		mse = mean_squared_error(prediction, Y_test)
		mae = mean_absolute_error(prediction, Y_test)
		mses.append(mse)
		maes.append(mae)

	avg_mse = sum(mses)/len(mses)
	avg_mae = sum(maes)/len(maes)

	print("Average MSE for all samples across",iterations,"models:",avg_mse)
	print("Average MAE for all samples across",iterations,"models:",avg_mae)