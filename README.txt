What I am doing:

Filtering data:

1. Remove rows that dont have valid open/close dates
2. Only include calls from a top 8 agency
3. The borogh must be specified
4. Change outliers to the median (an oulier is +-(8*median), or < 0)
5. Each file produced is for a specific agency and borough combo, denoted in the title of the file
The files produced has the median response times in hours, corresponding to the day

4 different GCRF structures:

	gcrf_allAgencies_response.py:

	1.	Produce one large graph which incorporates all agencies, and jointly predicts k days ahead for each agency.

	gcrf_one_agency_response.py:

	2.	Produce one graph for each agency. Predict k days ahead just for that agency.

	gcrf_one_agency_joint.py:

	3.	Produce one graph for each agency, with demand and response nodes adjacent.

	gcrf_combine.py:

	4.	Produce one graph for each agency, with demand and response nodes combined into a single node.


Linear regression:

	Use only the previous d days to fit a line. Predict the next k days using this line. Each agency has its own model.

RNN:
	Train and test an LSTM network on the same sequences used by GCRF.

TODO:
	
	- Add to exel a description next to each result, also add another document with observations
	- read paper on drive
	- run dataport on spiedie
	- find other papers that use multiple features in gcrf
	- change x_length and y_length
	- read about how correlated variables improve prediction

	- predict demand and use response to help
	- find distance of agents from call
	- add holidays
	- add weather?


