#write time series per complaints.
#Use data from 2015 - now
#Use complaint types that have at least 5 complaints per day (365*3)+58 days
#Remove complaints that have median response time less than 1 hour

import pandas as pd
import matplotlib.pyplot as plt
import csv
import sys
from datetime import datetime
import statistics as stat

#return a generator, so we dont have to load the whole file in memory
def read_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        for row in reader:
            yield row

#2012 and 2016 are leap years
def getDayNumber(year, month, day):
	year = year-2010 #2010 is the first year we have data for
	days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31]
	day_num = (year*365)
	if((year > 2) or ((year == 2) and (month > 2))): #2012 leap year
		day_num += 1
	if((year > 6) or ((year == 6) and (month > 2))): #2016 leap year
		day_num += 1
	for i in list(range(0,month-1)):
		day_num += days_in_months[i]
	day_num += day
	return(day_num)

def subtractDates(start,end):
	format = '%m/%d/%Y %I:%M:%S %p'
	start_date = datetime.strptime(start, format)
	end_date = datetime.strptime(end, format)
	difference = (end_date-start_date).total_seconds()
	return int(difference)

print("Finding Response Times...")

#start day in 2015
start_day = 366 + 365*4

complaints = pd.read_csv('311_Complaint_Types.csv')
complaints_list = complaints['Complaint Type'].tolist()

list_of_response_dicts = []
list_of_demand_dicts = []
for i in complaints_list:
	list_of_response_dicts.append({i+str(x):[] for x in range(1,1+(366)+(365*2)+58)})
	list_of_demand_dicts.append({i+str(x):0 for x in range(1,1+(366)+(365*2)+58)})

file_generator = read_csv('311_data.csv')

#index 1 is created date
#index 2 is closed date
for line in file_generator: #index 1 is created date
	if(line[1] != 'Created Date'):
		created_date = line[1]
		closed_date = line[2]
		complaint_type = line[5]
		if(closed_date != '' and complaint_type in complaints_list): #ignore rows that have no closed date
			response_time = int(subtractDates(created_date,closed_date)/3600) #in hours
			year = int(created_date.split()[0].split('/')[2])
			if(year >= 2015):
				month = int(created_date.split()[0].split('/')[0])
				day = int(created_date.split()[0].split('/')[1])
				day_num = getDayNumber(year,month,day) - start_day
				for index,item in enumerate(complaints_list):
					if(complaint_type == item):
						list_of_response_dicts[index][complaint_type+str(day_num)].append(response_time)
						list_of_demand_dicts[index][complaint_type+str(day_num)] += 1


#now replace each response time list with its median value
for d in list_of_response_dicts:
	for key,value in d.items():
		median = 0
		if(len(value) > 0):
			median = stat.median(value)
		d[key] = []
		d[key].append(median)

#extract a list of ordered response times from each dictionary, and write to csv
path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaint/' #write data here
fh = open("/home/mllab/Desktop/defazio-311_gcrf/stats/complaint_stats.txt","w") #write statistics here

required_demand = ((365*3)+58)*5

for i in range(0,len(complaints_list)):
	response_times = []
	demands = []
	index = 0
	complaint = ''
	for key in sorted(list_of_response_dicts[i]):
		if(index == 0):
			complaint = key[:-1]
		response_times.append(list_of_response_dicts[i][key][0])
		demands.append(list_of_demand_dicts[i][key])
		index += 1

	#only write to file if there is enough data, and median response is greater than 0
	median_response = stat.median(response_times)
	if(sum(demands) >= required_demand and median_response > 0):

		#change outliers in response and demand to the median
		median_demand = stat.median(demands)
		num_response_outliers = 0
		num_demand_outliers = 0
		for i in range(0,len(response_times)):
			if((response_times[i] > 8*median_response) or (response_times[i] < 0)):
				response_times[i] = median_response
				num_response_outliers += 1
			if((demands[i] > 8*median_demand) or (demands[i] < 0)):
				demands[i] = median_demand
				num_demand_outliers += 1


		fh.write("Complaint Type: "+complaint+'\n')
		fh.write("Median response: " +str(median_response)+'\n')
		fh.write("Response outliers: "+str(num_response_outliers)+'\n')
		fh.write("Demand outliers: "+str(num_demand_outliers)+'\n')
		fh.write("Total calls: "+str(sum(demands))+'\n\n')
		data = pd.DataFrame({'Response_Time':response_times, 'Demand':demands})
		print("writing csv...")

		#replace any /'s in complaint with _, so the path isn't confused
		complaint = complaint.replace('/','_')

		data.to_csv(path+complaint+'.csv')

fh.close()