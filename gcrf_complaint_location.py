#Model a single agency

import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs

#subtract by mean and divide by std (mean and std of all response times)
def normalize(data):
	std = np.std(response_times)
	mean = np.mean(response_times)
	for i in range(0,data.shape[0]):
		for j in range(0,data.shape[1]):
			data[i,j] = (data[i,j]-mean)/std
	return data

#multiply by std and add the mean (mean and std of all response times)
def denormalize(data):
	std = np.std(response_times)
	mean = np.mean(response_times)
	for i in range(0,data.shape[0]):
			for j in range(0,data.shape[1]):
				data[i,j] = ((std*data[i,j])+mean)
	return data


x_length = 50
y_length = 7
iterations = 3

complaints = ['Blocked Driveway', 'Dirty Conditions', 'HEAT_HOT WATER', 'Illegal Parking', 
			'Missed Collection (All Materials)', 'Noise - Residential', 
			'Sanitation Condition', 'Sewer', 'Street Condition', 
			'Traffic Signal Condition', 'UNSANITARY CONDITION', 'Water System']

boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaintLocation/'

#run for each complaint/borough seperately
for chosen_borough in boroughs:
	for chosen_complaint in complaints:
		filename = path+chosen_borough+'_'+chosen_complaint+'.csv'
		print("Running for:",chosen_borough,'|',chosen_complaint)
		response_times, X_train, Y_train, X_test, Y_test = gs.getSamples(
			filename, x_length, y_length)



		#X_train = normalize(X_train)
		#Y_train = normalize(Y_train)
		#X_test = normalize(X_test)

		model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000) #lamL and lamT are regularization parameters

		mses = []
		maes = []

		#run the model a few times and average the MSE
		for i in range(0,iterations):

			model.fit(X_train, Y_train)
			prediction = model.predict(X_test)
			#prediction = denormalize(prediction)

			#matrix MSE
			mse = mean_squared_error(prediction, Y_test)
			mae = mean_absolute_error(prediction, Y_test)
			mses.append(mse)
			maes.append(mae)

		avg_mse = sum(mses)/len(mses)
		avg_mae = sum(maes)/len(maes)

		print("Average MSE for all samples across",iterations,"models:",avg_mse)
		print("Average MAE for all samples across",iterations,"models:",avg_mae)