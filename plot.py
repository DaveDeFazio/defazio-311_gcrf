import matplotlib.pyplot as plt
import getSamples as gs
import numpy as np

#agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']
#boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']



#for chosen_agency in agencies:
#chosen_agency = agencies[0]
#chosen_borough = boroughs[0]

#complaints = pd.read_csv('311_Complaint_Types.csv')
#complaints_list = complaints['Complaint Type'].tolist()

chosen_complaint = 'HEAT_HOT WATER'
chosen_borough = 'BROOKLYN'

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaintLocation/'
filename = path+chosen_borough+'_'+chosen_complaint+'.csv'
responses, demands = gs.getResponseAndDemand(filename)
print("Avg response time for", chosen_complaint, np.mean(responses))

days = [x for x in range(0,len(responses))]
plt.plot(days, responses)
plt.plot(days, demands)#, color='orange'
plt.xlabel('Day')
plt.ylabel('Response times (in hours), Demand (in # of calls)')
plt.title('Demand and Median Response Times per Day for ...')
plt.legend()
plt.show()