import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs


x_length = 50
y_length = 7
iterations = 3

agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']
boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByAgencyLocation/'

#run for each agency seperately
for chosen_agency in agencies:

	print("Agency:",chosen_agency)

	filename = path + 'MANHATTAN_'+chosen_agency+'.csv'
	_, manhattan_x_train, manhattan_y_train, manhattan_x_test, manhattan_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + 'STATEN ISLAND_'+chosen_agency+'.csv'
	__, staten_x_train, staten_y_train, staten_x_test, staten_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + 'QUEENS_'+chosen_agency+'.csv'
	___, queens_x_train, queens_y_train, queens_x_test, queens_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + 'BRONX_'+chosen_agency+'.csv'
	___, bronx_x_train, bronx_y_train, bronx_x_test, bronx_y_test = gs.getSamples(
			filename, x_length, y_length)

	filename = path + 'BROOKLYN_'+chosen_agency+'.csv'
	___, brooklyn_x_train, brooklyn_y_train, brooklyn_x_test, brooklyn_y_test = gs.getSamples(
			filename, x_length, y_length)

	X_train = np.empty([manhattan_x_train.shape[0], 
		manhattan_x_train.shape[1] + staten_x_train.shape[1] + queens_x_train.shape[1]
		+ bronx_x_train.shape[1] + brooklyn_x_train.shape[1]])

	Y_train = np.empty([manhattan_y_train.shape[0], 
		manhattan_y_train.shape[1] + staten_y_train.shape[1] + queens_y_train.shape[1]
		+ bronx_y_train.shape[1] + brooklyn_y_train.shape[1]])

	X_test = np.empty([manhattan_x_test.shape[0], 
		manhattan_x_test.shape[1] + staten_x_test.shape[1] + queens_x_test.shape[1]
		+ bronx_x_test.shape[1] + brooklyn_x_test.shape[1]])


	X_train[:,0::5] = manhattan_x_train
	X_train[:,1::5] = staten_x_train
	X_train[:,2::5] = queens_x_train
	X_train[:,3::5] = bronx_x_train
	X_train[:,4::5] = brooklyn_x_train

	Y_train[:,0::5] = manhattan_y_train
	Y_train[:,1::5] = staten_y_train
	Y_train[:,2::5] = queens_y_train
	Y_train[:,3::5] = bronx_y_train
	Y_train[:,4::5] = brooklyn_y_train

	X_test[:,0::5] = manhattan_x_test
	X_test[:,1::5] = staten_x_test
	X_test[:,2::5] = queens_x_test
	X_test[:,3::5] = bronx_x_test
	X_test[:,4::5] = brooklyn_x_test

	model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000)

	manhattan_mses = []
	staten_mses = []
	queens_mses = []
	brooklyn_mses = []
	bronx_mses = []

	for i in range(0,iterations):

		model.fit(X_train, Y_train)
		predictions = model.predict(X_test)

		#every 5th y belongs to the same borough
		manhattan_prediction = predictions[:,0::5]
		staten_prediction = predictions[:,1::5]
		queens_prediction = predictions[:,2::5]
		bronx_prediction = predictions[:,3::5]
		brooklyn_prediction = predictions[:,4::5]

		manhattan_mses.append(mean_squared_error(manhattan_prediction, manhattan_y_test))
		staten_mses.append(mean_squared_error(staten_prediction, staten_y_test))
		queens_mses.append(mean_squared_error(queens_prediction, queens_y_test))
		bronx_mses.append(mean_squared_error(bronx_prediction, bronx_y_test))
		brooklyn_mses.append(mean_squared_error(brooklyn_prediction, brooklyn_y_test))

	print("Manhattan:",sum(manhattan_mses)/len(manhattan_mses))
	print("Staten:",sum(staten_mses)/len(staten_mses))
	print("Queens:",sum(queens_mses)/len(queens_mses))
	print("Bronx:",sum(bronx_mses)/len(bronx_mses))
	print("Brooklyn:",sum(brooklyn_mses)/len(brooklyn_mses))


"""
>>> import numpy as np
>>> a = np.array([[1,4,7],[1,4,7]])
>>> b = np.array([[2,5,8],[2,5,8]])
>>> c = np.array([[3,6,9],[3,6,9]])
>>> d = np.empty([a.shape[0],a.shape[1]+b.shape[1]+c.shape[1]])
>>> d[:,0::3] = a
>>> d[:,1::3] = b
>>> d[:,2::3] = c
>>> d
array([[1., 2., 3., 4., 5., 6., 7., 8., 9.],
       [1., 2., 3., 4., 5., 6., 7., 8., 9.]])

"""