import pandas as pd
import matplotlib.pyplot as plt
import csv
import sys
from datetime import datetime
import statistics as stat
import numpy as np

#find median response of the 261 complaint types, for 2017

#return a generator, so we dont have to load the whole file in memory
def read_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        for row in reader:
            yield row

#2012 and 2016 are leap years
def getDayNumber(year, month, day):
	year = year-2010 #2010 is the first year we have data for
	days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31]
	day_num = (year*365)
	if((year > 2) or ((year == 2) and (month > 2))): #2012 leap year
		day_num += 1
	if((year > 6) or ((year == 6) and (month > 2))): #2016 leap year
		day_num += 1
	for i in list(range(0,month-1)):
		day_num += days_in_months[i]
	day_num += day
	return(day_num)

def subtractDates(start,end):
	format = '%m/%d/%Y %I:%M:%S %p'
	start_date = datetime.strptime(start, format)
	end_date = datetime.strptime(end, format)
	difference = (end_date-start_date).total_seconds()
	return int(difference)

print("Finding Response Times...")

top_agencies = ['DEP','DOB','DOHMH','DOT','DPR','DSNY','HPD','NYPD']

complaints = pd.read_csv('311_Complaint_Types.csv')
complaints_list = complaints['Complaint Type'].tolist()
complaints_dict = {complaint:[] for complaint in complaints_list}
num_complaints = {complaint:0 for complaint in complaints_list}

file_generator = read_csv('311_data.csv')
csv_length = 0

#index 1 is created date
#index 2 is closed date
for line in file_generator: #index 1 is created date
	if(line[1] != 'Created Date'):
		created_date = line[1]
		closed_date = line[2]
		agency = line[3]
		complaint_type = line[5]
		year = int(created_date.split()[0].split('/')[2])
		#ignore rows that have no closed date, not a top 8 agency, or not a valid complaint type
		if(closed_date != '' and agency in top_agencies
			and complaint_type in complaints_list and year == 2017): 
			response_time = int(subtractDates(created_date,closed_date)/3600) #in hours
			complaints_dict[complaint_type].append(response_time)
			num_complaints[complaint_type] += 1
			


#now replace each response time list with its median value
for key,value in complaints_dict.items():
	median = 0
	if(len(value) > 0):
		median = stat.median(value)
	complaints_dict[key] = []
	complaints_dict[key].append(median)


fh = open("complaints_median_response_times.txt","w") #write statistics here
fh2 = open("number_complaints.txt","w")
for key in sorted(complaints_dict):
	if(complaints_dict[key][0] != 0):
		fh.write(key + ' ' + str(complaints_dict[key][0]) + '\n')

for key in sorted(num_complaints):
	if(num_complaints[key] != 0):
		fh2.write(key + ' ' + str(num_complaints[key]) + '\n')

fh.close()
fh2.close()