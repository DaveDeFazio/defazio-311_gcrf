import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import numpy as np
from sklearn.metrics import mean_squared_error


X_train = np.array([[1,2,3],
			[4,5,6],
			[7,8,9],
			[10,11,12],
			[13,14,15],
			[16,17,18],
			[19,20,21]])

Y_train = np.array([[1,2,3,4,5,6,7],
			[2,2,3,4,5,6,7],
			[3,2,3,4,5,6,7],
			[4,2,3,4,5,6,7],
			[5,2,3,4,5,6,7],
			[6,2,3,4,5,6,7],
			[7,2,3,4,5,6,7]])

X_test = np.array([[8,9,10],
			[18,19,20],
			[43,44,45]])

Y_test = np.array([[3,2,3,4,5,6,7],
			[10,2,3,4,5,6,7],
			[13,2,3,4,5,6,7]])

## CAN'T DO THIS
"""X_train = np.array([[[1,2,3],[2,3,4]],
			[[3,3,3],[3,4,5]],
			[[1,2,3],[4,5,6]]
			])

Y_train = np.array([[4,5],
			[7,8],
			[10,11]])

X_test = np.array([[[1,2,3],[2,3,4]],
			[[3,3,3],[3,4,5]],
			[[1,2,3],[4,5,6]]
			])

Y_test = np.array([[11,12],
			[21,22],
			[46,47]])"""


model = SparseGaussianCRF(lamL=0.1, lamT=0.1,n_iter=2)
model.fit(X_train, Y_train)
prediction = model.predict(X_test)

print(prediction)

mse = mean_squared_error(prediction,Y_test)
print("MSE:",mse)