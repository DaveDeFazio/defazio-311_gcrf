#Model a single agency

import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs

x_length = 50
y_length = 7
iterations = 3

complaints = ['Blocked Driveway', 'Dirty Conditions', 'HEAT_HOT WATER', 'Illegal Parking', 
			'Missed Collection (All Materials)', 'Noise - Residential', 
			'Sanitation Condition', 'Sewer', 'Street Condition', 
			'Traffic Signal Condition', 'UNSANITARY CONDITION', 'Water System']
boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaintLocation/'

for chosen_borough in boroughs:
	for chosen_complaint in complaints:

		filename = path+chosen_borough+'_'+chosen_complaint+'.csv'
		print("Running for:",chosen_borough,'|',chosen_complaint)

		response_times, X_train, Y_train, X_test, Y_test = gs.getSamplesWithDemand(
			filename, x_length, y_length)

		model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000) #lamL and lamT are regularization parameters

		response_mses = []
		demand_mses = []

		response_maes = []
		demand_maes = []


		#run the model a few times and average the MSE
		for i in range(0,iterations):

			model.fit(X_train, Y_train)
			prediction = model.predict(X_test)

			#response and demand predictions alternate
			response_predictions = prediction[:,0::2]
			demand_predictions = prediction[:,1::2]

			response_truth = Y_test[:,0::2]
			demand_truth = Y_test[:,1::2]

			#matrix MSE
			response_mse = mean_squared_error(response_predictions, response_truth)
			demand_mse = mean_squared_error(demand_predictions, demand_truth)
			response_mae = mean_absolute_error(response_predictions, response_truth)
			demand_mae = mean_absolute_error(demand_predictions, demand_truth)

			response_mses.append(response_mse)
			demand_mses.append(demand_mse)
			response_maes.append(response_mae)
			demand_maes.append(demand_mae)

		avg_mse_response = sum(response_mses)/len(response_mses)
		avg_mse_demand = sum(demand_mses)/len(demand_mses)
		avg_mae_response = sum(response_maes)/len(response_maes)
		avg_mae_demand = sum(demand_maes)/len(demand_maes)


		print("Average response MSE:",avg_mse_response)
		print("Average response MAE:",avg_mae_response)

