#https://www.incomebyzipcode.com/newyork/11368
#https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/7ahn-ypff

"""
How I am filtering:

1. Remove rows that dont have valid open/close dates, or valid zips
2. Change outliers to the median (an oulier is +-(8*median), or < 0)
3. Each file produced is for a specific agency, denoted in the title of the file
The files produced has the median response times in hours, corresponding to the day

"""

import pandas as pd
import matplotlib.pyplot as plt
import csv
import sys
from datetime import datetime
import statistics as stat

#return a generator, so we dont have to load the whole file in memory
def read_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        for row in reader:
            yield row

#2012 and 2016 are leap years
def getDayNumber(year, month, day):
	year = year-2010 #2010 is the first year we have data for
	days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31]
	day_num = (year*365)
	if((year > 2) or ((year == 2) and (month > 2))): #2012 leap year
		day_num += 1
	if((year > 6) or ((year == 6) and (month > 2))): #2016 leap year
		day_num += 1
	for i in list(range(0,month-1)):
		day_num += days_in_months[i]
	day_num += day
	return(day_num)

def subtractDates(start,end):
	format = '%m/%d/%Y %I:%M:%S %p'
	start_date = datetime.strptime(start, format)
	end_date = datetime.strptime(end, format)
	difference = (end_date-start_date).total_seconds()
	return int(difference)

print("Finding Response Times...")
#366 days in 2012 and 2016, 365 days in 2010, 2011, 2013, 2014, 2015, and 2017. Also using 58 days of 2018
top_agencies = ['DOHMH','NYPD','DSNY','DEP','DPR','DOB','DOT','HPD']
list_of_response_dicts = []
list_of_demand_dicts = []
for i in top_agencies:
	list_of_response_dicts.append({i+str(x):[] for x in range(1,1+(366*2)+(365*6)+58)})
	list_of_demand_dicts.append({i+str(x):0 for x in range(1,1+(366*2)+(365*6)+58)})

file_generator = read_csv('311_data.csv')
csv_length = 0

#index 1 is created date
#index 2 is closed date
for line in file_generator: #index 1 is created date
	csv_length += 1
	if(line[1] != 'Created Date'):
		created_date = line[1]
		closed_date = line[2]
		agency = line[3]
		if(closed_date != '' and agency in top_agencies): #ignore rows that have no closed date, or not a top 8 agency
			response_time = int(subtractDates(created_date,closed_date)/3600) #in hours
			year = int(created_date.split()[0].split('/')[2])
			month = int(created_date.split()[0].split('/')[0])
			day = int(created_date.split()[0].split('/')[1])
			day_num = getDayNumber(year,month,day)
			for index,item in enumerate(top_agencies):
				if(agency == item):
					list_of_response_dicts[index][agency+str(day_num)].append(response_time)
					list_of_demand_dicts[index][agency+str(day_num)] += 1

print("Total number of rows read:",csv_length)

#now replace each response time list with its median value
for d in list_of_response_dicts:
	num_days_no_data = 0
	for key,value in d.items():
		median = 0
		if(len(value) > 0):
			median = stat.median(value)
		else:
			num_days_no_data += 1
		d[key] = []
		d[key].append(median)
	print("No data for",num_days_no_data,"total days")

#extract a list of ordered response times from each dictionary, and write to csv
time = [x for x in range(1,1+(366*2)+(365*6)+58)]
path = '/home/mllab/Desktop/defazio-311_gcrf/dataByAgency/' #write data here
fh = open("agency_stats.txt","w") #write statistics here

for i in range(0,len(top_agencies)):
	response_times = []
	demands = []
	index = 0
	agency = ''
	for key in sorted(list_of_response_dicts[i]):
		if(index == 0):
			agency = key[:-1]
		response_times.append(list_of_response_dicts[i][key][0])
		demands.append(list_of_demand_dicts[i][key])
		index += 1

	#change outliers in response and demand to the median
	median_response = stat.median(response_times)
	median_demand = stat.median(demands)
	num_response_outliers = 0
	num_demand_outliers = 0
	for i in range(0,len(response_times)):
		if((response_times[i] > 8*median_response) or (response_times[i] < 0)):
			response_times[i] = median_response
			num_response_outliers += 1
		if((demands[i] > 8*median_demand) or (demands[i] < 0)):
			demands[i] = median_demand
			num_demand_outliers += 1


	fh.write("Agency: "+agency+'\n')
	fh.write("Response outliers: "+str(num_response_outliers)+'\n')
	fh.write("Demand outliers: "+str(num_demand_outliers)+'\n')
	fh.write("Total calls: "+str(sum(demands))+'\n\n')
	"""plt.plot(time, response_times)
	plt.plot(time, demands)
	plt.xlabel('Day')
	plt.ylabel('Median Response Time (blue) and Demands (orange)')
	plt.title('311 Median Response Times and Demands for 2010 - 2018')
	plt.show()"""
	data = pd.DataFrame({'Response_Time':response_times, 'Demand':demands})
	print("writing csv...")
	data.to_csv(path+agency+'.csv')

fh.close()