#write time series per complaints.
#Use data from 2017
#Use complaint types that have at least 5 complaints per hour (365*24*5)
#Remove complaints that have median response time less than 1 hour
#Split also by borough

import pandas as pd
import matplotlib.pyplot as plt
import csv
import sys
from datetime import datetime
import statistics as stat

#return a generator, so we dont have to load the whole file in memory
def read_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        for row in reader:
            yield row

#2012 and 2016 are leap years
def getDayNumber(year, month, day):
	year = year-2010 #2010 is the first year we have data for
	days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31]
	day_num = (year*365)
	if((year > 2) or ((year == 2) and (month > 2))): #2012 leap year
		day_num += 1
	if((year > 6) or ((year == 6) and (month > 2))): #2016 leap year
		day_num += 1
	for i in list(range(0,month-1)):
		day_num += days_in_months[i]
	day_num += day
	return(day_num)

def subtractDates(start,end):
	format = '%m/%d/%Y %I:%M:%S %p'
	start_date = datetime.strptime(start, format)
	end_date = datetime.strptime(end, format)
	difference = (end_date-start_date).total_seconds()
	return int(difference)

#start day in 2017 (2 leap years and 5 normal years passed)
start_hour = ((366*2) + (365*5))*24

complaints = pd.read_csv('311_Complaint_Types.csv')
complaints_list = complaints['Complaint Type'].tolist()

boroughs = ['MANHATTAN', 'STATEN ISLAND', 'QUEENS', 'BRONX', 'BROOKLYN']

#end of 2017
end_hour = start_hour + (365*24)

#initialize dictionary of dictionaries to store all time series
all_time_series_response = {borough+'_'+complaint:{x:[] for x in range(start_hour,end_hour+1)} 
	for borough in boroughs for complaint in complaints_list}
all_time_series_demand = {borough+'_'+complaint:{x:0 for x in range(start_hour,end_hour+1)} 
	for borough in boroughs for complaint in complaints_list}

print("Finding Response Times...")

#store each calls response time and demand in appropriate dictionary
file_generator = read_csv('311_data.csv')

for line in file_generator:
	if(line[1] != 'Created Date'):
		created_date = line[1]
		closed_date = line[2]
		complaint_type = line[5]
		borough = line[24]
		year = int(created_date.split()[0].split('/')[2])
		if(closed_date != '' and complaint_type in complaints_list
			and borough != 'Unspecified' and year == 2017): #ignore rows that have no closed date
			response_time = int(subtractDates(created_date,closed_date)/3600) #in hours
			month = int(created_date.split()[0].split('/')[0])
			day = int(created_date.split()[0].split('/')[1])
			inner_key = getDayNumber(year,month,day)*24 #hour index of call
			outer_key = borough+'_'+complaint_type #location and complaint of call
			all_time_series_response[outer_key][inner_key].append(response_time)
			all_time_series_demand[outer_key][inner_key] += 1

#now replace each response time list with its median value
for borough in boroughs:
	for complaint in complaints_list:
		median = 0
		for i in range(start_hour,end_hour+1):
			outer_key = borough+'_'+complaint
			inner_key = i
			if(len(all_time_series_response[outer_key][inner_key]) > 0):
				median = stat.median(all_time_series_response[outer_key][inner_key])
			all_time_series_response[outer_key][inner_key] = []
			all_time_series_response[outer_key][inner_key].append(median)

#at least 5 complaints for each hour of 2017
required_demand = 365*24*5

#write to csv
path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaintLocationHour/' #write data here
fh = open("/home/mllab/Desktop/defazio-311_gcrf/stats/location_complaint_hour_stats.txt","w") #write statistics here
for outer_key in all_time_series_response.keys():
	response_times = []
	demands = []

	#inner key must be in order
	for inner_key in all_time_series_response[outer_key].keys():
		response_times.append(all_time_series_response[outer_key][inner_key][0])
		demands.append(all_time_series_demand[outer_key][inner_key])

	median_response = stat.median(response_times)
	median_demand = stat.median(demands)

	if(sum(demands) >= required_demand and median_response > 0 and median_demand > 0):
		#change outliers in response and demand to the median
		num_response_outliers = 0
		num_demand_outliers = 0
		for k in range(0,len(response_times)):
			if((response_times[k] > 8*median_response) or (response_times[k] < 0)):
				response_times[k] = median_response
				num_response_outliers += 1
			if((demands[k] > 8*median_demand) or (demands[k] < 0)):
				demands[k] = median_demand
				num_demand_outliers += 1


		fh.write(outer_key+'\n')
		fh.write("Response outliers: "+str(num_response_outliers)+'\n')
		fh.write("Demand outliers: "+str(num_demand_outliers)+'\n')
		fh.write("Number of total calls: "+str(sum(demands))+'\n\n')

		data = pd.DataFrame({'Response_Time':response_times, 'Demand':demands})

		#replace any /'s in complaint with _, so the path isn't confused
		outer_key = outer_key.replace('/','_')

		print("writing csv...")
		data.to_csv(path+outer_key + '.csv')

fh.close()
