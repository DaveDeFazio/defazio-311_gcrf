import numpy as np
import sys
sys.path.append("/home/mllab/Desktop/defazio-311_gcrf/sgcrfpy-master")
from sgcrf import SparseGaussianCRF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import getSamples as gs

x_length = 50
y_length = 7
iterations = 2


complaints = ['APPLIANCE','Air Quality','Animal Abuse', 'Animal in a Park',
			'Asbestos', 'Blocked Driveway', 'Broken Muni Meter', 'Building_Use', 
			'Consumer Complaint', 'Curb Condition', 
			'DCA _ DOH New License Application Request','DOF Literature Request', 
			'DOF Parking - Payment Issue', 'DOF Property - Owner Issue', 
			'DOF Property - Payment Issue', 'DOF Property - Reduction Issue', 
			'DOF Property - Request Copy', 'DOF Property - Update Account', 
			'DOOR_WINDOW', 'Damaged Tree', 'Dead_Dying Tree', 'Derelict Vehicle',
			'Dirty Conditions', 'ELECTRIC', 'Electrical', 'Elevator', 'FLOORING_STAIRS', 
			'Food Establishment', 'Food Poisoning', 'For Hire Vehicle Complaint', 
			'GENERAL', 'General Construction_Plumbing', 'Graffiti', 'HEAT_HOT WATER', 
			'HPD Literature Request', 'Hazardous Materials', 'Highway Condition', 
			'Homeless Encampment', 'Housing - Low Income Senior', 'Illegal Parking', 
			'Indoor Air Quality', 'Lead', 'Litter Basket _ Request',
			'Maintenance or Facility', 'Missed Collection (All Materials)',
			'New Tree Request', 'Noise - Commercial', 'Noise - Park', 
			'Noise - Residential', 'Noise - Street_Sidewalk', 'Noise - Vehicle',
			'Noise', 'Non-Emergency Police Matter', 'Other Enforcement',
			'Overgrown Tree_Branches', 'PAINT_PLASTER', 'PLUMBING', 'Plumbing', 'Rodent', 
			'Root_Sewer_Sidewalk Condition', 'SAFETY', 'SCRIE', 'Sanitation Condition',
			'Sewer', 'Sidewalk Condition', 'Special Projects Inspection Team (SPIT)',
			'Standing Water', 'Street Condition', 'Street Sign - Damaged', 
			'Street Sign - Dangling', 'Street Sign - Missing', 'Taxi Complaint', 
			'Traffic Signal Condition', 'Traffic', 'UNSANITARY CONDITION', 
			'Unsanitary Animal Pvt Property', 'Vacant Lot', 'Vending', 
			'WATER LEAK', 'Water Conservation', 'Water System']

path = '/home/mllab/Desktop/defazio-311_gcrf/dataByComplaint/'

for chosen_complaint in complaints:

	filename = path+chosen_complaint+'.csv'
	print("Running for:",chosen_complaint)

	response_times, X_train, Y_train, X_test, Y_test = gs.getSamplesWithDemand(
		filename, x_length, y_length)

	model = SparseGaussianCRF(lamL=0.1, lamT=0.1, n_iter=10000) #lamL and lamT are regularization parameters

	response_mses = []
	demand_mses = []

	response_maes = []
	demand_maes = []


	#run the model a few times and average the MSE
	for i in range(0,iterations):

		model.fit(X_train, Y_train)
		prediction = model.predict(X_test)

		#response and demand predictions alternate
		response_predictions = prediction[:,0::2]
		demand_predictions = prediction[:,1::2]


		response_truth = Y_test[:,0::2]
		demand_truth = Y_test[:,1::2]


		#matrix MSE
		response_mse = mean_squared_error(response_predictions, response_truth)
		demand_mse = mean_squared_error(demand_predictions, demand_truth)
		response_mae = mean_absolute_error(response_predictions, response_truth)
		demand_mae = mean_absolute_error(demand_predictions, demand_truth)

		response_mses.append(response_mse)
		demand_mses.append(demand_mse)
		response_maes.append(response_mae)
		demand_maes.append(demand_mae)

	avg_mse_response = sum(response_mses)/len(response_mses)
	avg_mse_demand = sum(demand_mses)/len(demand_mses)
	avg_mae_response = sum(response_maes)/len(response_maes)
	avg_mae_demand = sum(demand_maes)/len(demand_maes)


	print("Average response MSE:",avg_mse_response)
	#print("Average demand MSE for all samples across",iterations,"models:",avg_mse_demand)
	print("Average response MAE:",avg_mae_response)