AGENCY 383
APPLIANCE 313
ATF 219.0
Adopt-A-Basket 49.5
Air Quality 69.0
Animal Abuse 3.0
Animal in a Park 13.0
Asbestos 2.0
BEST/Site Safety 69.0
Beach/Pool/Sauna Complaint 1180.0
Bike Rack Condition 285
Bike/Roller/Skate Chronic 1
Blocked Driveway 3.0
Boilers 181.0
Bottled Water 337
Bridge Condition 39.0
Broken Muni Meter 163
Broken Parking Meter 306.0
Building Condition 25
Building/Use 700
Bus Stop Shelter Placement 452
Calorie Labeling 79
Collection Truck Noise 20
Consumer Complaint 1104.0
Cooling Tower 986
Cranes and Derricks 36
Curb Condition 184.0
DCA / DOH New License Application Request 5526.0
DOF Literature Request 914
DOOR/WINDOW 290.0
Damaged Tree 192
Damaged or Dead Tree 1844
Dead/Dying Tree 193
Derelict Bicycle 29.0
Derelict Vehicle 4
Dirty Conditions 48
Disorderly Youth 2
Drinking 2.0
Drinking Water 979.0
Drug Activity 2
ELECTRIC 240.0
ELEVATOR 206
Electrical 96
Electronics Waste 48.0
Elevator 130.0
Emergency Response Team (ERT) 35
FATF 203
FCST 463
FLOORING/STAIRS 289.0
Ferry Complaint 26
Ferry Inquiry 36
Ferry Permit 23
Food Establishment 1455
Food Poisoning 24
Found Property 1138
GENERAL 280.0
General Construction/Plumbing 38
Graffiti 1199.0
HEAT/HOT WATER 60
HPD Literature Request 24
Harboring Bees/Wasps 283
Hazardous Materials 1.0
Highway Condition 17
Highway Sign - Damaged 126
Highway Sign - Dangling 264.5
Highway Sign - Missing 292.5
Homeless Encampment 2.0
Illegal Animal - Sold/Kept 1980
Illegal Fireworks 1.0
Illegal Parking 2
Illegal Tree Damage 714.0
Indoor Air Quality 47.0
Indoor Sewage 612
Industrial Waste 2
Interior Demo 178
Investigations and Discipline (IAD) 109
Lead 24.0
Lifeguard 1465
Litter Basket / Request 25.0
Maintenance or Facility 145.0
Miscellaneous Categories 278
Missed Collection (All Materials) 44
Mold 40
Mosquitoes 21.0
Municipal Parking Facility 169.5
New Tree Request 765
Noise 93
Noise - Commercial 1
Noise - House of Worship 1.0
Noise - Park 1
Noise - Residential 2
Noise - Street/Sidewalk 2.0
Noise - Vehicle 2.0
Non-Emergency Police Matter 2
OUTSIDE BUILDING 260
Other Enforcement 49
Overflowing Litter Baskets 23.5
Overflowing Recycling Baskets 41.5
Overgrown Tree/Branches 1060
PAINT/PLASTER 247
PLUMBING 255.0
Panhandling 2.0
Parking Card 1088
Plumbing 150
Posting Advertisement 2.0
Public Toilet 33.0
Radioactive Material 389.0
Recycling Enforcement 76
Request Large Bulky Item Collection 55
Request Xmas Tree Collection 26
Rodent 144
Root/Sewer/Sidewalk Condition 402.0
SAFETY 267.0
Sanitation Condition 26.0
Scaffold Safety 13
Sewer 4.0
Sidewalk Condition 77.0
Smoking -1028938
Snow 70.0
Special Natural Area District (SNAD) 468
Special Projects Inspection Team (SPIT) 532
Squeegee 2.0
Standing Water 216.0
Street Condition 29.0
Street Sign - Damaged 111
Street Sign - Dangling 189.0
Street Sign - Missing 128.0
Sweeping/Inadequate 26.0
Sweeping/Missed 34
Sweeping/Missed-Inadequate -6719
Tanning 406
Taxi Complaint 978.0
Traffic 1.0
Traffic Signal Condition 1
Tunnel Condition 38
UNSANITARY CONDITION 310
Unleashed Dog -1028432.0
Unsanitary Animal Pvt Property 168.0
Unsanitary Pigeon Condition 183.0
Urinating in Public 2
Vacant Lot 264
Vending 2.0
WATER LEAK 291.0
Water Conservation 4.0
Water Quality 310.0
Water System 8
X-Ray Machine/Equipment 525
